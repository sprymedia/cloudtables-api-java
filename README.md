# CloudTables API client

This library can be used by Java applications to interface with the CloudTables API. At the moment the only facility provided it to request access security tokens. These tokens can then be used to securely request a CloudTable interface for a specific table, with the access rights defined by the security key and requests from this library.

For full details on installing and using this library, please refer to [the CloudTables documentation](//cloudtables.com/docs/api/java).
