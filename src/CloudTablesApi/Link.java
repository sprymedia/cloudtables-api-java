package CloudTablesApi;

public class Link
{
    public String id;
    public String name;
    public Target target;
    public DP[] computed;
    public DP[] datapoints;
    public Link[] links;
}