package CloudTablesApi;

import java.lang.reflect.Field;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.KeyManagementException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.Gson;
import java.util.ArrayList;

public class RequestUtil
{
    private String _key;
    private String _role;
    private String _subdomain;
    private String _domain;
    private boolean _secure;
    private boolean _ssl;
    private ArrayList<String> _roles;
    
    public RequestUtil(String key, boolean ssl, String subdomain, String domain) {
        this._roles = new ArrayList<String>();
        this._key = key;
        this._ssl = ssl;
        this._subdomain = subdomain;
        this._domain = domain;
    }
    
    public Datasets requestDatasets(String path, String type) {
        return this.parseDatasets(this.response(path, type));
    }
    
    private Datasets parseDatasets(String json) {
        Gson gson = new Gson();
        return (Datasets)gson.fromJson(json, Datasets.class);
    }
    
    public AccessResponse requestAccessResponse(String path, String type, AccessRequest data) {
        return this.parseAccessResponse(this.response(path, type, data));
    }
    
    public Object requestDatasetData(String path, String type) {
        return this.parseDatasetData(this.response(path, type));
    }
    
    public <T> T requestRowData(String path, String type, Class<T> target) {
        System.out.println(target);
        return this.parseRowData(this.response(path, type), target);
    }
    
    public <S, T> T requestRowUpdate(String path, String type, S data, Class<T> target) {
        return this.parseRowData(this.response(path, type, data), target);
    }
    
    public Schema requestSchema(String path, String type) {
        return this.parseSchema(this.response(path, type));
    }
    
    public void insert(String path, String type, Object data) {
        this.response(path, type, data);
    }
    
    public void domain(String value) {
        this._domain = value;
    }
    
    public void secure(boolean state) {
        this._secure = state;
    }
    
    private String response(String path, String type) {
        RequestResponse resp = new RequestResponse(new AccessRequest());
        resp.key = this._key;
        try {
            String strURL = String.valueOf(this._ssl ? "https" : "http") + "://" + ((this._subdomain.length() > 0) ? (String.valueOf(this._subdomain) + "." + this._domain) : (String.valueOf(this._domain) + "/io")) + path;
            String params = "key=" + this._key;
            strURL = String.valueOf(strURL) + "?" + params;
            strURL = String.valueOf(strURL) + "&separator=_";
            URL url = new URL(strURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            if (!this._secure) {
                TrustModifier.relaxHostChecking(con);
            }
            con.setDoOutput(true);
            con.setRequestMethod(type);
            con.setRequestProperty("User-Agent", "CloudTables API Java client");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("charset", "utf-8");
            con.setRequestProperty("Content-Length", Integer.toString(params.length()));
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuffer content = new StringBuffer();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            return content.toString();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        catch (KeyManagementException e3) {
            e3.printStackTrace();
        }
        catch (NoSuchAlgorithmException e4) {
            e4.printStackTrace();
        }
        catch (KeyStoreException e5) {
            e5.printStackTrace();
        }
        return null;
    }
    
    private String response(String path, String type, AccessRequest data) {
        RequestResponse resp = new RequestResponse(data);
        resp.key = this._key;
        if (this._roles.size() > 0) {
            resp.roles = this._roles;
        }
        if (this._role != null) {
            (resp.roles = new ArrayList<String>()).add(this._role);
        }
        String res = this.toForm(data, data.getClass(), "", true);
        try {
            String strURL = String.valueOf(this._ssl ? "https" : "http") + "://" + ((this._subdomain.length() > 0) ? (String.valueOf(this._subdomain) + "." + this._domain) : (String.valueOf(this._domain) + "/io")) + path;
            if (type.equals("GET")) {
                Pattern p = Pattern.compile(Pattern.quote("\"conditions\":\\[.*\\]"));
                Matcher m = p.matcher(res);
                if (m.find()) {
                    String justCons = m.group(0);
                    strURL = String.valueOf(strURL) + "&" + justCons;
                }
            }
            URL url = new URL(strURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            if (!this._secure) {
                TrustModifier.relaxHostChecking(con);
            }
            con.setDoOutput(true);
            con.setRequestMethod(type);
            con.setRequestProperty("User-Agent", "CloudTables API Java client");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("charset", "utf-8");
            String params = "key=" + this._key;
            if (data != null && data.conditions != null) {
                for (int i = 0; i < data.conditions.size(); ++i) {
                    Condition cond = data.conditions.get(i);
                    if (cond.id != null) {
                        params = String.valueOf(params) + "&conditions[" + i + "][id]=" + data.conditions.get(i).id;
                    }
                    else if (cond.name != null) {
                        params = String.valueOf(params) + "&conditions[" + i + "][name]=" + data.conditions.get(i).name;
                    }
                    if (cond.value != null) {
                        params = String.valueOf(params) + "&conditions[" + i + "][value]=" + data.conditions.get(i).value;
                    }
                    if (cond.op != null) {
                        params = String.valueOf(params) + "&conditions[" + i + "][op]=" + data.conditions.get(i).op;
                    }
                    if (cond.setValue != null) {
                        params = String.valueOf(params) + "&conditions[" + i + "][setValue]=" + data.conditions.get(i).setValue;
                    }
                    params = String.valueOf(params) + "&conditions[" + i + "][set]=" + data.conditions.get(i).set;
                }
            }
            if (data.clientId != null) {
                params = String.valueOf(params) + "&clientId=" + data.clientId;
            }
            if (data.clientName != null) {
                params = String.valueOf(params) + "&clientName=" + data.clientName;
            }
            if (data.duration != 0) {
                params = String.valueOf(params) + "&duration=" + data.duration;
            }
            if (data.role != null) {
                params = String.valueOf(params) + "&clientId=" + data.clientId;
            }
            if (data.roles != null) {
                for (int i = 0; i < data.roles.size(); ++i) {
                    params = String.valueOf(params) + "&roles[" + i + "]=" + data.roles.get(i);
                }
            }
            con.setRequestProperty("Content-Length", Integer.toString(params.length()));
            byte[] outputInBytes = params.getBytes("UTF-8");
            OutputStream os = con.getOutputStream();
            os.write(outputInBytes);
            os.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuffer content = new StringBuffer();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            return content.toString();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        catch (KeyManagementException e3) {
            e3.printStackTrace();
        }
        catch (NoSuchAlgorithmException e4) {
            e4.printStackTrace();
        }
        catch (KeyStoreException e5) {
            e5.printStackTrace();
        }
        return null;
    }
    
    private <T> String response(String path, String type, T data) {
        String res = this.toForm(data, data.getClass(), "", true);
        try {
            String strURL = String.valueOf(this._ssl ? "https" : "http") + "://" + ((this._subdomain.length() > 0) ? (String.valueOf(this._subdomain) + "." + this._domain) : (String.valueOf(this._domain) + "/io")) + path;
            if (type.equals("GET")) {
                Pattern p = Pattern.compile(Pattern.quote("\"conditions\":\\[.*\\]"));
                Matcher m = p.matcher(res);
                if (m.find()) {
                    String justCons = m.group(0);
                    strURL = String.valueOf(strURL) + "&" + justCons;
                }
            }
            strURL = String.valueOf(strURL) + "?key=" + this._key;
            strURL = String.valueOf(strURL) + "&separator=_";
            URL url = new URL(strURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            if (!this._secure) {
                TrustModifier.relaxHostChecking(con);
            }
            con.setDoOutput(true);
            con.setRequestMethod(type);
            con.setRequestProperty("User-Agent", "CloudTables API Java client");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("charset", "utf-8");
            con.setRequestProperty("Content-Length", Integer.toString(res.length()));
            byte[] outputInBytes = res.getBytes("UTF-8");
            OutputStream os = con.getOutputStream();
            os.write(outputInBytes);
            os.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuffer content = new StringBuffer();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            return content.toString();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        catch (KeyManagementException e3) {
            e3.printStackTrace();
        }
        catch (NoSuchAlgorithmException e4) {
            e4.printStackTrace();
        }
        catch (KeyStoreException e5) {
            e5.printStackTrace();
        }
        return null;
    }
    
    private AccessResponse parseAccessResponse(String json) {
        Gson gson = new Gson();
        return (AccessResponse)gson.fromJson(json, AccessResponse.class);
    }
    
    private Object parseDatasetData(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Object.class);
    }
    
    private <T> T parseRowData(String json, Class<T> target) {
        Gson gson = new Gson();
        System.out.println(json);
        System.out.println(target);
        return (T)gson.fromJson(json, target);
    }
    
    private Schema parseSchema(String json) {
        Gson gson = new Gson();
        return (Schema)gson.fromJson(json, Schema.class);
    }
    
    private <T> String toForm(T obj, Class target, String keyPath, boolean first) {
        String result = "";
        Field[] fieldlist = target.getDeclaredFields();
        for (int i = 0; i < fieldlist.length; ++i) {
            Field fld = fieldlist[i];
            fld.setAccessible(true);
            Class type = fld.getType();
            if (!type.equals(String.class)) {
                if (!type.equals(Integer.TYPE)) {
                    try {
                        if (fld != null && obj != null && fld.get(obj) != null) {
                            result = result.concat(this.toForm(fld.get(obj), fld.getDeclaringClass(), String.valueOf(keyPath) + "[" + fld.getName() + "]", false));
                        }
                    }
                    catch (IllegalArgumentException ex2) {}
                    catch (IllegalAccessException ex3) {}
                    continue;
                }
            }
            try {
                if (fld.get(obj) != null) {
                    result = result.concat(String.valueOf(keyPath) + (first ? "" : "[") + fld.getName() + (first ? "=" : "]="));
                    if (type.equals(String.class)) {
                        result = result.concat((String)fld.get(obj));
                    }
                    else {
                        result = result.concat(Integer.toString((int)fld.get(obj)));
                    }
                    result = result.concat("&");
                }
            }
            catch (IllegalArgumentException | IllegalAccessException e) {
            }
        }
        return result;
    }
}