package CloudTablesApi;

public class Schema
{
    public DP[] computed;
    public DP[] datapoints;
    public String description;
    public Link[] links;
    public String name;
    public Table[] table;
}