package CloudTablesApi;

public class Row
{
    private int _id;
    private Dataset _host;
    private RequestUtil _requestUtil;
    
    public Row(Dataset host, int id) {
        this._host = host;
        this._id = id;
        this._requestUtil = new RequestUtil(this._host.key(), this._host.ssl(), this._host.subdomain(), this._host.domain());
    }
    
    public <T> T data(Class<T> target) {
        return this._requestUtil.requestRowData("/api/1/dataset/" + this._host.id() + "/" + this._id, "GET", target);
    }
    
    public <T> T delete(Class<T> target) {
        return this._requestUtil.requestRowData("/api/1/dataset/" + this._host.id() + "/" + this._id, "DELETE", target);
    }
    
    public int id() {
        return this._id;
    }
    
    public <S, T> T update(S data, Class<T> target) {
        return this._requestUtil.requestRowUpdate("/api/1/dataset/" + this._host.id() + "/" + this._id, "PUT", data, target);
    }
}