package CloudTablesApi;

import java.util.ArrayList;

public class RequestResponse
{
    String key;
    ArrayList<String> roles;
    String clientId;
    String clientName;
    int duration;
    ArrayList<Condition> conditions;
    
    public RequestResponse(AccessRequest access) {
        if (access != null) {
            this.clientId = access.clientId;
            this.clientName = access.clientName;
            this.duration = access.duration;
            this.conditions = access.conditions;
        }
        this.roles = new ArrayList<String>();
    }
}