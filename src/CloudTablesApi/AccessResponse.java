package CloudTablesApi;

import java.util.ArrayList;

public class AccessResponse
{
    public String token;
    ArrayList<AccessResponse.Error> errors;
    
    public AccessResponse(String token) {
        this.token = token;
        this.errors = new ArrayList<AccessResponse.Error>();
    }

    private class Error {
        String msg;
        String name;
    }
}