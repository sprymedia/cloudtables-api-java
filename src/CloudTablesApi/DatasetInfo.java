/*! CloudTables API client
 * �SpryMedia Ltd - MIT licensed
 */

package CloudTablesApi;

public class DatasetInfo {
    public int deleteCount;
    public String id;
    public int insertCount;
    public String lastData;
    public String name;
    public int readCount;
    public int rowCount;
    public int updateCount;
}