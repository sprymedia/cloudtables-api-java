/*! CloudTables API client
 * �SpryMedia Ltd - MIT licensed
 */

package CloudTablesApi;

public class Column {
    public String id;
    public String link;
    public String name;
    public String data;
}