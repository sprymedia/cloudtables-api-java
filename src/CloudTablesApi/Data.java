/*! CloudTables API client
 * �SpryMedia Ltd - MIT licensed
 */

package CloudTablesApi;

public class Data {
    public int id;
    public int dv;
    public String color;
    public int s;
}