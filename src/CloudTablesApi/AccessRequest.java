package CloudTablesApi;

import java.util.ArrayList;

public class AccessRequest
{
    String clientId;
    String clientName;
    int duration;
    ArrayList<Condition> conditions;
    ArrayList<String> roles;
    String role;
}