/*! CloudTables API client
 * �SpryMedia Ltd - MIT licensed
 */

package CloudTablesApi;

import java.util.ArrayList;

public class DatasetData
{
    public ArrayList<Column> columns;
    public ArrayList<Data> data;
}