/*! CloudTables API client
 * �SpryMedia Ltd - MIT licensed
 */

package CloudTablesApi;

import java.util.ArrayList;

public class CloudTablesApi {

	private String _accessToken;
	private String _clientId = null;
	private String _clientName = null;
	private ArrayList<Condition> _conditions = new ArrayList<Condition>();
	private String _domain = "cloudtables.io";
	private int _duration = -1;
	private String _key;
	private Boolean _secure = true;
	private ArrayList<String> _roles = new ArrayList<String>();
	private String _role;
	private Boolean _ssl = true;
	private String _subdomain;
	private RequestUtil _requestUtil;

	public CloudTablesApi(String key) {
		this._subdomain = "";
		this._key = key;
		this._requestUtil = new RequestUtil(key, (boolean)this._ssl, this._subdomain, this._domain);
	}

	public CloudTablesApi(String subdomain, String key) {
		this._subdomain = subdomain;
		this._key = key;
		this._requestUtil = new RequestUtil(key, (boolean)this._ssl, this._subdomain, this._domain);
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	* Public methods
	*/

	/**
	 * Your unique identifier for the user
	 * @param value Client ID
	 * @return Self for chaining
	 */
	public CloudTablesApi clientId(String value) {
		this._clientId = value;

		return this;
	}

	/**
	 * Name / label to give the use in the CloudTables configuration UI
	 * @param value Client name
	 * @return Self for chaining
	 */
	public CloudTablesApi clientName(String value) {
		this._clientName = value;
		
		return this;
	}

	/**
	 * Add a condition to the data that is to be fetched
	 * @param cond an instance of the Condition class containing the details of the condition to be applied
	 * @return CloudTablesApi for chaining
	 */
	public CloudTablesApi condition(Condition cond) {
		this._conditions.add(cond);

		return this;
	}

	/**
	* Get the data and columns for a given dataset
	* @param dataset ID (UUID) of the dataset to be shown
	*/
	public Object data(String dataset) {
		return new Dataset(dataset, this._accessToken, (boolean)this._ssl, this._subdomain, this._domain, this._key).data();
	}

	/**
	* Get summary information about the available datasets
	*/
	public Datasets datasets() {
		return this._requestUtil.requestDatasets("/api/1/datasets", "GET");
	}
	
	public Dataset dataset(String id) {
		return new Dataset(id, this.token(), (boolean)this._ssl, this._subdomain, this._domain, this._key);
	}

	/**
	* Host domain for the CloudTables instance
	* @param value TLD
	* @return Self for chaining
	*/
	public CloudTablesApi domain(String value) {
		this._domain = value;
		this._requestUtil.domain(value);
		return this;
	}

	/**
	 * Token expire duration
	 * @param value Duration
	 * @return Self for chaining
	 */
	public CloudTablesApi duration(int value) {
		this._duration = value;
		
		return this;
	}

	/**
	 * Add a role to the access rights (union'ed with the roles for the API key)
	 * @param value Role name
	 * @return Self for chaining
	 */
	public CloudTablesApi role(String value) {
		this._roles.add(value);
		
		return this;
	}

	/**
	 * Set multiple roles for the access rights (union'ed with the roles for the API key)
	 * @param value Role names
	 * @return Self for chaining
	 */
	public CloudTablesApi roles(ArrayList<String> value) {
		this._roles = value;
		
		return this;
	}

	/**
	* Get a `<script>` tag for a specific dataset to be displayed
	* @param datasetId ID (UUID) of the dataset to be shown
	* @param style The styling framework to use for this table
	* @returns `<script>` tag to use.
	*/
	public String scriptTag(String datasetId) {
		String token = this.token();

		return this.scriptTag(token, datasetId, "d");
	}

	/**
	* Get a `<script>` tag for a specific dataset to be displayed
	* @param datasetId ID (UUID) of the dataset to be shown
	* @param style The styling framework to use for this table
	* @returns `<script>` tag to use.
	*/
	public String scriptTag(String datasetId, String style) {
		String token = this.token();

		return this.scriptTag(token, datasetId, style);
	}

	/**
	* Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
	* where you want the table to appear in your HTML.
	* @param token Secure access token (from `.token()`)
	* @param datasetId ID (UUID) of the dataset to be shown
	* @param style The styling framework to use for this table
	* @returns `<script>` tag to use.
	*/
	public String scriptTag(String token, String datasetId, String style) {
		String[] styles = new String[] {
			"d",
			"auto",
			"bs",
			"bs4",
			"dt",
			"zf",
			"ju",
			"se",
			"no"
		};

		boolean styAcc = false;

		for (int i = 0; i < styles.length; i++) {
			if (styles[i].equals(style)) {
				styAcc = true;
			}
		}

		if (!styAcc) {
			return null;
		}
		
		return "<script src=\"" + (this._ssl ? "https" : "http") + "://" + (this._subdomain.length() > 0 ? this._subdomain  + "." + this._domain : this._domain + "/io") + "/loader/" + datasetId + "/table/" + style + "\" data-token=\"" + token + "\"></script>";
	}

	/**
	 * Allow self signed certificates (`false`) or not (`true` - default).
	 * @param value Flag
	 * @return Self for chaining
	 */
	public CloudTablesApi secure(boolean state) {
		this._secure = state;
		this._requestUtil.secure(state);
		return this;
	}

	/**
	 * Use https (`true`) or http (`false`) connections (for self hosted installs).
	 * @param value Flag
	 * @return Self for chaining
	 */
	public CloudTablesApi ssl(boolean active) {
		this._ssl = active;
		return this;
	}

	/**
	* Get an access token. Note that this value is cached - a single token per instance is
	* all that is required.
	* @return Access token
	*/
	public String token() {
		if (this._accessToken != null) {
			return this._accessToken;
		}

		AccessRequest data = new AccessRequest();

		if (this._duration != data.duration) {
			data.duration = this._duration;
		}

		if (this._clientId != null) {
			data.clientId = this._clientId;
		}

		if (this._clientName != null) {
			data.clientName = this._clientName;
		}

		if (this._conditions.size() > 0) {
			data.conditions = this._conditions;
		}

		if(this._role != null) {
			data.role = this._role;
		}

		if(this._roles != null) {
			data.roles = this._roles;
		}

		// Save - in case used multiple times
		this._accessToken = this._requestUtil.requestAccessResponse("/api/1/access", "POST", data).token;

		return this._accessToken;
	}
}