/*! CloudTables API client
 * �SpryMedia Ltd - MIT licensed
 */

package CloudTablesApi;

public class Condition {
    public String id;
    public String name;
    public String op;
    public boolean set;
    public String setValue;
    public String value;

    /**
     * Constructor to initialise a condition with an id and a value
     * @param id
     * @param value
     */
    public Condition(String id, String value) {
        this.id = id;
        this.name = null;
        this.op = "=";
        this.set = true;
        this.setValue = null;
        this.value = value;
    }

    /**
     * Setter for the column id for this condition
     * @param id the column id used by CT
     * @return self for chaining
     */
    public Condition id(String id) {
        this.id = id;
        this.name = null;

        return this;
    }

    /**
     * Setter for the column name for this condition
     * @param name the column name used by CT
     * @return self for chaining
     */
    public Condition name(String name) {
        this.name = name;
        this.id = null;

        return this;
    }

    /**
     * Setter for the operation to be used for this condition
     * @param op the operation to be used
     * @return self for chaining
     */
    public Condition op(String op) {
        this.op = op;

        return this;
    }

    /**
     * Setter for the whether set is to be enabled for this condition
     * @param set boolean indicating to set or not
     * @return self for chaining
     */
    public Condition set(boolean set) {
        this.set = set;

        return this;
    }

    /**
     * Setter for setValue for this condition
     * @param setValue the value to be written into the database
     * @return self for chaining
     */
    public Condition setValue(String setValue) {
        this.setValue = setValue;

        return this;
    }

    /**
     * Setter for the value to be used for the condition
     * @param value the value to be used 
     * @return self for chaining
     */
    public Condition value(String value) {
        this.value = value;

        return this;
    }
}