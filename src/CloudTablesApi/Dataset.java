package CloudTablesApi;

public class Dataset
{
    public CloudTablesApi ctAPI;
    private String _domain;
    private String _token;
    private boolean _ssl;
    private String _subdomain;
    private String _id;
    private String _key;
    private RequestUtil _requestUtil;
    
    public Dataset(String id, String token, boolean ssl, String subdomain, String domain, String key) {
        this._id = id;
        this._token = token;
        this._ssl = ssl;
        this._subdomain = subdomain;
        this._domain = domain;
        this._key = key;
        this._requestUtil = new RequestUtil(key, this._ssl, this._subdomain, this._domain);
    }
    
    public Object data() {
        return this._requestUtil.requestDatasetData("/api/1/dataset/" + this._id + "/data", "GET");
    }
    
    public String id() {
        return this._id;
    }
    
    public String key() {
        return this._key;
    }
    
    public boolean ssl() {
        return this._ssl;
    }
    
    public String subdomain() {
        return this._subdomain;
    }
    
    public String domain() {
        return this._domain;
    }
    
    public Dataset insert(Object data) {
        this._requestUtil.insert("/api/1/dataset/" + this.id(), "POST", data);
        return this;
    }
    
    public Row row(int id) {
        return new Row(this, id);
    }
    
    public Schema schema() {
        return this._requestUtil.requestSchema("/api/1/dataset/" + this.id() + "/schema", "GET");
    }
    
    public String scriptTag() {
        return this.scriptTag(this._token, this._id, "d");
    }
    
    public String scriptTag(String datasetId, String style) {
        return this.scriptTag(this._token, datasetId, style);
    }
    
    public String scriptTag(String token, String datasetId, String style) {
        String[] styles = { "d", "auto", "bs", "bs4", "dt", "zf", "ju", "se", "no" };
        boolean styAcc = false;
        for (int i = 0; i < styles.length; ++i) {
            if (styles[i].equals(style)) {
                styAcc = true;
            }
        }
        if (!styAcc) {
            return null;
        }
        return "<script src=\"" + (this._ssl ? "https" : "http") + "://" + ((this._subdomain.length() > 0) ? (String.valueOf(this._subdomain) + "." + this._domain) : (String.valueOf(this._domain) + "/io")) + "/loader/" + datasetId + "/table/" + style + "\" data-token=\"" + token + "\"></script>";
    }
    
    public String scriptTagAsync() {
        return "tag";
    }
}